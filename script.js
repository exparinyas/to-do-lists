const dateTime = document.getElementById('date-time');
const addText = document.getElementById('add__text');
const listItem = document.getElementById('lists-item');
const refreshBtn = document.getElementById('refresh-btn');
const addBtn = document.getElementById('add-btn');
const togClass = document.getElementById('toggle-class');
const addMessage = document.getElementById('add_message');



// รับ วัน-เดือน-วันที่ จากเครื่อง มาแสดงบนหน้าจอแอพ
let date = new Date();
const options = {
    month: 'long',                                          // long คือ แสดงเดือนแบบเต็ม
    weekday: 'long',                                        // long คือ แสดงวันแบบเต็ม
    day: '2-digit'                                          // 2-digit คือ แสดงวันที่แบบเลข 2 หลัก
}
let today = date.toLocaleDateString('en-US', options);      // en-US คือ แสดงวันเวลาเป็นภาษาอังกฤษ
dateTime.innerHTML = today;



// เมื่อ Focus Tag Input ที่มี id add__text จะทำ Function นี้
let focusText = () => {
    togClass.classList.add('height-focus');      // add class height-focus เข้าไปใน tag ที่มี id toggle-class
    addText.maxLength = '30';                    // เมื่อ focus tag ที่มี id add_Text ให้ maxLength คือ กำหนดให้สามารถใส่ตัวอักษรได้ตามที่กำหนด
}



// กำหนดให้ Tag Input รับตัวอักษรได้ตามที่กำหนดไว้
let messageAlertForAddText = () => {
    if(event.key !== 'Enter') {                                 // เมื่อกดปุ่มใดๆ ที่ไม่ใช้ปุ่ม Enter จะทำงานใน if
        let a = addText.value;                                  // รับค่าจาก tag input
        let b = a.length;                                       // นำค่าที่ได้จาก tag input มานับตัวอักษรได้กี่ตัว
        
        if(b === 30) {                                          // ถ้าตัวอักษรที่ได้จาก tag input มี 30 ตัว จะทำใน if
            addMessage.classList.add('message-alert');          // add class message-alert เข้าไปใน tag ที่มี id add_message
            setTimeout(function() {                             // เมื่อเวลาผ่านไป 2 วินาที จะทำการ remove class message-alert ออกจาก tag ที่มี id add_message
                addMessage.classList.remove('message-alert');
            }, 2000);                                           // 1000 miliseconds = 1 seconds
        }
    }
}



// แบบคลิกปุ่ม ADD TEXT ที่หน้าจอแอพ
let inputValue = () => {
    let textInput = addText.value.trim();   // trim คือ เมื่อเจอเว้นวรรค หัว-ท้าย ข้อความจะตัดออก

    if(textInput === '') {                  // ถ้าค่าทีได้เท่ากับค่าว่างจะ return ออกทันที
        return;
    }

    let listInput = `<li id="lists-item-child">
                        <div id="add_debug_message">
                            <i class="fas fa-exclamation-triangle"></i>
                            <span>กรอกได้ไม่เกิน 30 ตัวอักษร</span>
                        </div>
                        <input type="checkbox" id="checkbox__text">
                        <p id="item">${textInput}</p>
                        <input type="text" id="debug__text">
                        <button id="edit-btn" class="list-btn-icon">
                            <i class="fas fa-pen-square"></i>
                        </button>
                        <button id="save-btn" class="list-btn-icon">
                            <i class="fas fa-save"></i>
                        </button>
                        <button id="delete-btn" class="list-btn-icon">
                            <i class="fas fa-trash"></i>
                        </button>
                    </li>`;
                    
    listItem.insertAdjacentHTML('afterbegin', listInput);   // วิธี add tag html แบบ object ใหญ่ๆ หลายๆตัว
    addText.value = '';                                     // add ข้อความเสร็จแล้วก็ให้ tag input เท่ากับค่าว่าง
    
    const listsItemChild = document.getElementById('lists-item-child');
    const addDebugMessage = document.getElementById('add_debug_message');
    const checkBox = document.getElementById('checkbox__text');
    const item = document.getElementById('item');
    const debugText = document.getElementById('debug__text');
    const editBtn = document.getElementById('edit-btn');
    const saveBtn = document.getElementById('save-btn');
    const deleteBtn = document.getElementById('delete-btn');

    // เมื่อทำการคลิกปุ่ม Check จะทำ Function นี้
    let checkList = () => {
        item.classList.toggle('line-through');   // toggle class line-through ให้กับ tag ที่มี id item สลับไปมาได้
    }

    // เมื่อทำการคลิกปุ่ม Edit จะทำ Function นี้
    let editThis = () => {
        debugText.classList.add('edit-mode');   // add class edit-mode ให้กับ tag ที่มี id debug__text
        saveBtn.classList.add('edit-mode');     // add class edit-mode ให้กับ tag ที่มี id save-btn
        item.classList.add('edit-mode');        // add class edit-mode ให้กับ tag ที่มี id item
        editBtn.classList.add('edit-mode');     // add class edit-mode ให้กับ tag ที่มี id edit-btn

        let pValue = item.textContent;          // ดึง text ที่อยู่ใน tag ที่มี id item ออกมาเก็บไว้ในตัวแปร pValue
        debugText.value = pValue;               // ให้ tag ที่มี id debug__text มี value เท่ากับค่า pValue ที่ดึงมา
        debugText.focus();                      // ให้ focus ที่ tag ที่มี id debug__text
        debugText.maxLength = '30';             // maxLength คือ กำหนดให้สามารถใส่ตัวอักษรได้ตามที่กำหนด
    }

    // กำหนดให้ Tag Input รับตัวอักษรได้ตามที่กำหนดไว้
    let messageAlertForDebugText = () => {
        if(event.key !== 'Enter') {                                             // เมื่อกดปุ่มใดๆ ที่ไม่ใช้ปุ่ม Enter จะทำงานใน if
            let a = debugText.value;                                            // รับค่าจาก tag input
            let b = a.length;                                                   // นำค่าที่ได้จาก tag input มานับตัวอักษรได้กี่ตัว

            if(b === 30) {                                                      // ถ้าตัวอักษรที่ได้จาก tag input มี 30 ตัว จะทำใน if
                addDebugMessage.classList.add('message-debug-alert');           // add class message-debug-alert เข้าไปใน tag ที่มี id add_debug_message
                setTimeout(function() {                                         // เมื่อเวลาผ่านไป 2 วินาที จะทำการ remove class message-debug-alert ออกจาก tag ที่มี id add_debug_message
                    addDebugMessage.classList.remove('message-debug-alert');
                }, 2000);                                                       // 1000 miliseconds = 1 seconds
            }
        }
    }

    // เมื่อทำการคลิกปุ่ม Save จะทำ Function นี้
    let saveThis = () => {
        let paraGraph = debugText.value.trim();     // trim คือ เมื่อเจอเว้นวรรค หัว-ท้าย ข้อความจะตัดออก

        if(paraGraph) {                             // ถ้า paraGraph มีค่า debugText.value อยู่จริง ก็คือมี value ใน input จริง ก็จะทำใน if แต่ถ้าไม่มีค่า value ใน input ก็จะไม่ทำใน if แล้วก็ทำบรรทัดถัดไปคือลบ class ที่เป็น input ออกแล้วโชว์ <p> ที่เป็นข้อความเดิม
            item.textContent = paraGraph;
        }

        debugText.classList.remove('edit-mode');    // remove class edit-mode ออกจาก tag ที่มี id debug__text
        saveBtn.classList.remove('edit-mode');      // remove class edit-mode ออกจาก tag ที่มี id save-btn
        item.classList.remove('edit-mode');         // remove class edit-mode ออกจาก tag ที่มี id item
        editBtn.classList.remove('edit-mode');      // remove class edit-mode ออกจาก tag ที่มี id edit-btn
    }

    // เมื่อทำการกดปุ่ม Enter จะทำ Function นี้
    let enterSave = (event) => {
        let e = event.keyCode;      // ดักจับ event จาก keyborad เมื่อกดปุ่ม keyborad จะ return ค่าเป็น keyCode ของปุ่มนั้น

        if(e === 13) {              // 13 คือ unicode ของปุ่ม Enter
            saveThis();
        }
    }
    
    // เมื่อทำการคลิกปุ่ม Remove จะทำ Function นี้
    let removeThis = () => {
        listsItemChild.remove();    // remove tag ที่มี id lists-item-child
    }

    checkBox.addEventListener('change', checkList);
    editBtn.addEventListener('click', editThis);
    saveBtn.addEventListener('click', saveThis);
    deleteBtn.addEventListener('click', removeThis);
    debugText.addEventListener('keyup', enterSave);
    debugText.addEventListener('keypress', messageAlertForDebugText);
}



// กดปุ่ม Enter เพื่อ ADD TEXT
let keyDownEnter = (event) => {
    let e = event.keyCode;      // ดักจับ event จากหน้าจอ เมื่อกด keyborad แล้วรับค่า unicode ที่ได้เก็บไว้ในตัวแปร e / keyCode จะ return ค่าเป็น number

    if(e === 13) {              // ตรวจสอบว่าค่าที่ได้จากการกด keyborad ที่นำค่า unicode ที่เก็บไว้ในตัวแปร e นั้นมีค่าเท่ากับ 13 หรือไม่ ถ้าจริงทำใน if
        inputValue();
    }
}



// กดปุ่ม Reset เพื่อ Reset ทุกอย่าง
let removeAll = () => {
    while(listItem.hasChildNodes()) {               // hasChildNodes() จะ return ค่าเป็น boolean ในบรรทัดนี้คือใน listsItem มี node ลูกไหม? ถ้ามีก็ทำ loop while
        listItem.removeChild(listItem.firstChild);  // ลบ node ลูกตัวแรกของ listItem และวน loop ไปเรื่อยๆ
    }

    togClass.classList.remove('height-focus');      // remove class height-focus ออกจาก tag ที่มี id toggle-class
    addText.value = '';
}



addText.addEventListener('focus', focusText);
addText.addEventListener('keyup', keyDownEnter);
addText.addEventListener('keypress', messageAlertForAddText);
addBtn.addEventListener('click', inputValue);
refreshBtn.addEventListener('click', removeAll);